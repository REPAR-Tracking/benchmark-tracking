# Bentta -- Benchmarking tool of tracking algorithms

This package helps automate tracking simulations and generate reports.

## Screenshots
<img src="img/errCenter_Crossing_Screenshots.png"/>
<img src="img/overlapRate_Crossing_Screenshots.png"/>
<img src="img/successRateErrCenter_Crossing_Screenshots.png"/>
<img src="img/successRateOverlapRate_Crossing_Screenshots.png"/>

## Features

- Automates all your tracking simulations
- Generates plots, frames and videos automatically
- Allows you to average results over multiple runs
- Sort trackers by performance
- Able to compare the same tracker with different parameters

## Quick try

This package is bundled with a short sequence and a dummy tracker. Run
`main.m` in the `tests` directory and look at the generated plots,
frames and videos in the directories `tests/plots`, `tests/frames` and
`tests/videos` respectively.

## Configuring and running some simulations

You first need to specify the path of this package in your script:
```matlab
addpath(<package_path>);
```
To be able to run some simulations, you then need to specify the
sequences you will be using as a cell array:
```matlab
seqs = {struct('name', <sequence_name>,...
               'path', <path_to_images>,...
               'anno', <path_to_groundtruth>)};
```
If you don't have the groundtruth file, you can specify the first
bounding box in a `initRect` field. The format of the image files is
customizable through the field `format`. See [here](#conf_seq) for
full specification.

Next, you have to specify the trackers to use in your simulations.
```matlab
trackers = {struct('name', 'MyAwesomeTracker', 'path', <tracker_path>)};
```
Each tracker is supposed to be located in the directory `path` and
called using the function `run_MyAwesomeTracker`.  This is of course
customizable, see [here](#conf_trk) for full details. By default this
function is called with two arguments: the sequence and the tracker
structure itself. Another common calling convention is supported.

And finally configure your simulations:
```matlab
smls = {};
```
which will run all configured trackers on all configured sequences. To
be more specific a simulation consists in a sequence and a set of
trackers. For example:
```matlab
smls = {struct('sequence', <sequence_name>,...
               'trackers', {{<tracker_name1>, <tracker_name2>}})};
```
By default if no trackers are specified, all available trackers are used.

You are now ready to run the simulations:
```matlab
runSimulation(smls, seqs, trackers);
```
Optionally, a fourth argument can be supplied to specify various
output paths and some global parameters (see [here](#global_param) for
details).

## Reporting the results

There are several built-in way of exporting the data. One can generate
success rate, overlap rate and centre error plots, videos and frames
specified in a `type` field. First, a single export:
```matlab
expts = {struct('sequence', <sequence_name>,...
                'type', 'errCenter')};
```
that opens a figure with the centre error plot.

This figure can be automatically exported to a `png` image file by
specifying an `export` field:
```matlab
expts = {struct('sequence', <sequence_name>,...
                'type', 'errCenter',...
                'export', 'png')};
```
If the script `matlab2tikz` is in your path, your can export the plot
in Tikz as well:
```matlab
expts = {struct('sequence', <sequence_name>,...
                'type', 'errCenter',...
                'export', {{'png', 'tikz'}})};
```

Now that the exports are configured, run:
```matlab
runExports(expts, seqs, trackers);
```
and see how your tracker beats them all!

## Full specification
### <a name="global_param"></a>Global parameters

The functions `runExports` and `runSimulation` can be called with an
optional fourth argument that stores various extra parameters in a
structure.

The field `debugOnError` controls whether or not you enter in the
debugger if a tracker raises an error. This is useful for debugging
purposes. By default, the error is catched and the simulations continue.

You can specify the number of runs globally for each tracking task
instead of specifying it for each simulation by setting the field
`runs`.

By default, this program tries to resume from previous runs and does
not clobber already computed results. If you want to change this
behaviour and erase all results, set `overwrite` to true.

Another useful behaviour is controlled by the `dryrun` property that,
when set to true behaves as if `overwrite` were true without actually
writing down any results.

You can specify where various data files would be stored by tweaking
the following fields:
- `resultsPath` stores the raw results
- `perfsPath` stores the performance results
- `framesPath` stores the exported frames
- `plotsPath` stores the exported plots in case you export them as png
  images or pgfplots tex files
- `videosPath` stores the exported videos
By default, those directories are created at the same level of the
file calling `runSimulation`.

### <a name="conf_trk"></a>Configuring the trackers

The available trackers are represented as a cell array of structures.
Each element is a structure that accepts the following fields:
- `name` (mandatory): The name of the tracker that should be unique
- `variant` (optional): A name of a variant of a tracker if the same
  tracker is used more than once
- `path` (mandatory): The directory where the tracker is located
- `func` (optional): The function to call to run the tracker (by
  default `run_<name>` function is called)
- `callingConvention` (optional): Specify how the tracker is called
- `overwrite` (optional, default: no): Do not try to resume and
  overwrite results file
- `runs` (optional): Number of runs
- `color` (optional): Color for plots, frames and videos per tracker
- `style` (optional): Style for plots, frames and videos per tracker
- `extraRectangleArgs` (optional): Key-value arguments used to draw
  the tracker bounding box
- `legendName` (optional): Legend name of tracker to use instead of
  the one built from the name and variant

### <a name="conf_seq"></a>Configuring the sequences

The available sequences are represented as a cell array of structures.
Each element is a structure that has the following fields:
- `name` (mandatory): The name of the sequence
- `path` (mandatory): The directory where the images of the sequence
  are stored
- `format` (optional): The format of the filenames of the images
- `anno` (optional): The full path of the groundtruth bounding boxes
- `initRect` (optional): The first rectangle used to start tracking in
  case of `anno` is not available
- `startFrame` (optional): The first frame to start with.
- `endFrame` (optional): The last frame to use.
- `color` (optional): Color for frames and videos for groundtruth
- `style` (optional): Style for frames and videos per groundtruth
- `extraRectangleArgs` (optional): Key-value arguments used to draw
  the groundtruth bounding box

### Configuring the simulations

The simulations to conduct are represented as a cell array of
structures. Each element is a structure that accepts the following
fields:
- `sequence` (mandatory): Sequence name to use
- `trackers` (optional): List of trackers to use
- `runs` (optional): Number of runs

### Configuring the exports

The exports are represented as a cell array of structures. Each
element is a structure that accepts the following fields:

General purpose fields:
- `sequence` (mandatory): The sequence to use
- `type` (mandatory): List of exports among `errCenter`,
  `overlapRate`, `successRateErrCenter`, `successRateOverlapRate`,
  `video` and `frame`
- `trackers` (optional, default: all): List of trackers to use
- `trackersMax` (optional, default: infinity): Maximum number of
  trackers to display
- `selectedRuns` (optional, default: all): Select runs on which to
  average

Plots-related fields:
- `title` (optional): Title of plot
- `fontSize` (optional, default: 10): General font size for title,
  labels and legend
- `sequenceRange` (optional): Range of frames to use in the sequence
- `export` (optional): Set of subexport among `png`, `jpg`, `jpeg`,
  `tiff` or `tikz` if `matlab2tikz` is installed
- `figName` (optional): Name of exported file
- `successRateErrCenterRange` (optional, default: 0:.5:50): Range used
  for success rate of centre error plot
- `successRateOverlapRateRange` (optional, default: 0:.05:1): Range
  used for success rate of overlap rate plot
- `sort` (optional, default: `best first`): Whether or not to sort the
  trackers in plots (`best first` or `worst first`)
- `extraLegendArgs` (optional): Key-value properties to configure legends
- `xLabelName` (optional): Label on x-axis of plot
- `extraxLabelName` (optional): Key-value properties for x-label
  (overrides `extraLabelName`)
- `yLabelName` (optional): Label on y-axis of plot
- `extrayLabelName` (optional): Key-value properties for y-label
  (overrides `extraLabelName`)
- `extraLabelName` (optional): Key-value properties to configure labels
- `extraTikzArgs` (optional): Extra options to pass to `matlab2tikz`

Videos and frames related fields:
- `videoRange` (optional): The range used to select the frames to be
  incorporated in the video
- `frameRange` (optional): Index or array of indexes of frames to export
- `figName` (optional): Name of exported frame files
- `videoName` (optional): Name of exported video file
- `groundtruth` (optional, default: no): Whether or not to include the
  groundtruth bounding boxes
- `number` (optional, default: no): Write index on frames
- `average` (optional, default: yes): Average bounding boxes over
  `selectedRuns`
- `sort` (optional, default: no sorting): Whether or not to sort the
  drawing of bounding boxes (`best center error first`, `worst center
  error first`, `best overlap first`, `worst overlap first`)
- `extraRectangleArgs` (optional): Key-value properties when drawing
  bounding boxes
- `extraTextArgs` (optional): Key-value properties when writing index
  on frames

## History

## License

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see http://www.gnu.org/licenses/.

See COPYING for details.

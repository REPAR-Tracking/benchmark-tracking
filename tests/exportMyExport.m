function exportMyExport(expt, seqs, trks, parameters, exptType)
%EXPORTMYEXPORT Outputs the fps of trackers on chosen sequences

% Get trackers specified in export
usedTrks = getUsedTrackers(expt, trks);

% Get sequences specified in export
usedSeqs = getUsedSequences(expt, seqs);

fpsTable = [];
for idxTrk = 1:length(usedTrks)
    for idxSeq = 1:length(usedSeqs)
        trk = usedTrks{idxTrk};
        trkSName = getTrackerSnakeName(trk);

        seq = usedSeqs{idxSeq};
        seqName = seq.name;

        % Start at frame number 1 by default
        if ~isfield(seq, 'startFrame')
            seq.startFrame = 1;
        end

        if ~isfield(seq, 'endFrame')
            seq.endFrame = getEndFrame(seq);
        end
        seq.len = seq.endFrame - seq.startFrame + 1;

        filePath = fullfile(parameters.resultsPath, getResultsFileName(seqName, trkSName));

        try
            load(filePath, 'fullResult');
            fullResult.durations;
            duration = mean(fullResult.durations);
        catch
            error('Unable to load file ''%s'', skipping\n', filePath);
        end

        fpsTable(idxSeq, idxTrk) = seq.len / duration;
    end
end

% Average over all sequences
plot(mean(fpsTable, 1));

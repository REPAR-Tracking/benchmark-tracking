% Path to runSimulation and runExports
addpath('..');
addpath('~/repositories/matlab2tikz/src');

p = mfilename('fullpath');
path = fileparts(p);

% Populate 10 variants of dummy tracker
trackers = {};
for i = 1:10
    trackers{i} = struct('name', ['Tracker ', num2str(i)],...
                         'path', '.',...
                         'sigma', 5*i,...
                         'func', 'dummyTracker');
end

% No simulation means all possible simulations
smls = {};

% Only one sequence
seqs = {struct('name', 'Crossing_Screenshots',...
               'path', fullfile(path, 'Crossing', 'img'),...
               'anno', fullfile(path, 'Crossing', 'groundtruth_rect.txt'))};

clear parameters;
parameters.overwrite = true;
parameters.runs = 100;

% Run the simulations
runSimulation(smls, seqs, trackers, parameters);

expts = {struct('sequence', 'Crossing_Screenshots',...
                'export', 'tikz',...
                'extraTikzArgs', {{'standalone', true}},...
                'frameRange', [10, 20, 30],...
                'type', {{'errCenter', 'overlapRate', 'successrateerrcenter', 'successrateoverlap', 'frame'}})};

% Generate the configured exports
addpath('..');
runExports(expts, seqs, trackers);

cd('plots');
system('LD_LIBRARY_PATH= pdflatex -interaction=nonstopmode errCenter_Crossing_Screenshots.tex');
system('LD_LIBRARY_PATH= pdflatex -interaction=nonstopmode overlapRate_Crossing_Screenshots.tex');
system('LD_LIBRARY_PATH= pdflatex -interaction=nonstopmode successRateErrCenter_Crossing_Screenshots.tex');
system('LD_LIBRARY_PATH= pdflatex -interaction=nonstopmode successRateOverlapRate_Crossing_Screenshots.tex');

system('LD_LIBRARY_PATH= convert -density 75  errCenter_Crossing_Screenshots.pdf ../../img/errCenter_Crossing_Screenshots.png');
system('LD_LIBRARY_PATH= convert -density 75  overlapRate_Crossing_Screenshots.pdf ../../img/overlapRate_Crossing_Screenshots.png');
system('LD_LIBRARY_PATH= convert -density 75  successRateErrCenter_Crossing_Screenshots.pdf ../../img/successRateErrCenter_Crossing_Screenshots.png');
system('LD_LIBRARY_PATH= convert -density 75  successRateOverlapRate_Crossing_Screenshots.pdf ../../img/successRateOverlapRate_Crossing_Screenshots.png');

function results = dummyTracker(seq, trk)

% Fake tracker that uses the groundtruth to track ;)

rectAnno = dlmread(seq.anno);
rectAnno = rectAnno(seq.startFrame:seq.endFrame, :);

if isfield(trk, 'type')
    type = trk.type;
else
    type = 'noise';
end

switch type
  case 'shift'
    rectAnno(2:end, 1:2) = rectAnno(2:end, 1:2) + trk.sigma / 5;
  case 'ratio'
    r = (trk.sigma - 5) / 45 / 8 + 7/8;
    rectAnno(2:end, 3:4) = rectAnno(2:end, 3:4) * r;
  otherwise
    noise = cumsum(randn(size(rectAnno(2:end, :))));
    rectAnno(2:end, :) = rectAnno(2:end, :) + .01 * trk.sigma * noise;
end

pause(trk.sigma / 1000);

clear results;
results.res = rectAnno;
results.type = 'rect';


function runSimulation(smls, seqs, trksAll, varargin)
%RUNSIMULATION Run the configured simulations.
% The simulations are stored in SMLS. SEQS in the set of available
% sequences and TRKSALL the set of available trackers. An optional
% parameter that mainly stores paths can be supplied.

% Location of this file
    p = mfilename('fullpath');
    path = fileparts(p);
    addpath(fullfile(path, 'utils'));
    addpath(fullfile(path, 'utils', 'vendor'));

    if length(varargin) > 0
        parameters = varargin{1};
    else
        parameters = struct;
    end

    % Get various path from PARAMETERS
    resultsPath = getFieldDefault(parameters, 'resultsPath', './results');
    perfsPath = getFieldDefault(parameters, 'perfsPath', './perfs');
    trackersPath = getFieldDefault(parameters, 'trackersPath', '.');

    % ...and store them in PARAMETERS if not present
    parameters.resultsPath = resultsPath;
    parameters.perfsPath = perfsPath;
    parameters.trackersPath = trackersPath;

    exist(resultsPath, 'dir') || mkdir(resultsPath);
    exist(perfsPath, 'dir') || mkdir(perfsPath);

    % Store current working directory to restore it after the tracking is
    % done
    currentPath = pwd;

    fprintf('%d trackers configured\n', length(trksAll));
    fprintf('%d sequences configured\n', length(seqs));

    % All possible simulations if no simulation
    if length(smls) == 0
        smls = {};
        for i = 1:length(seqs)
            smls{i}.sequence = seqs{i}.name;
        end
    end

    % Flatten simulations in case one simulation struct is an array of
    % struct
    smls_flatten = {};
    for i = 1:length(smls)
        smls_flatten = [smls_flatten, num2cell(smls{i})];
    end
    smls = smls_flatten;

    % Compute the number of tracking task per simulation
    numTaskTotal = zeros(1, length(smls));
    for i = 1:length(smls)
        numRuns = getFieldDefault(parameters, 'runs', getFieldDefault(smls{i}, 'runs', 1));
        trks = getUsedTrackers(smls{i}, trksAll);
        for idxTrk = 1:length(trks)
            numRunsTrk = getFieldDefault(trks{idxTrk}, 'runs', numRuns);
            numTaskTotal(i) = numTaskTotal(i) + numRunsTrk;
        end
    end
    fprintf('%d simulations, %d tracking tasks\n', length(smls), sum(numTaskTotal));
    numTask = 1;

    for idxSml = 1:length(smls)

        % Update the number of tasks already done
        numTask = sum(numTaskTotal(1:idxSml-1)) + 1;

        sml = smls{idxSml};
        seqName = sml.sequence;
        fprintf('Run simulations on ''%s'' (%d/%d), overall (%d/%d)\n',...
                seqName, idxSml, length(smls), numTask, sum(numTaskTotal));

        seq = getSequenceFromName(seqName, seqs);
        if ~isstruct(seq)
            fprintf('  Sequence ''%s'' not found, skipping\n', seqName)
            continue;
        end

        % Default format of image files
        if ~isfield(seq, 'format')
            seq.format = '%04d.jpg';
        end

        % Start at frame number 1 by default
        if ~isfield(seq, 'startFrame')
            seq.startFrame = 1;
        end

        if ~isfield(seq, 'endFrame')
            seq.endFrame = getEndFrame(seq);
        end
        seq.len = seq.endFrame - seq.startFrame + 1;

        % Extract initial rectangle from groundtruth file or initRect field
        if ~isfield(seq, 'initRect')
            if isfield(seq, 'anno')
                rectAnno = dlmread(seq.anno);
                seq.initRect = rectAnno(seq.startFrame, :);
            else
                fprintf('  No ''initRect'' or ''anno'' for sequence ''%s'', skipping\n', seqName);
                continue;
            end
        end

        % Storing frames in seq.frames
        seqPath = getFieldDefault(seq, 'path', seq.name);
        for i = 1:seq.len
            fileName = sprintf(seq.format, i + seq.startFrame - 1);
            seq.frames{i} = fullfile(seqPath, fileName);
        end

        % Compatibility with Wu et al conventions
        seq.s_frames = seq.frames;
        seq.init_rect = seq.initRect;

        % Retrieve trackers to be used for current simulation SML
        [trks, trkNames, notFound] = getUsedTrackers(sml, trksAll);
        if ~isempty(notFound)
            trkNames = cellfun(@(x) getTrackerFancyName(x), notFound, 'UniformOutput', false);
            fprintf('  WARNING: Some trackers have not been found: ''%s''\n', strjoin(trkNames));
        end

        % Compute number of runs for each tracker
        numRuns = getFieldDefault(parameters, 'runs', getFieldDefault(sml, 'runs', 1));

        for idxTrk = 1:length(trks)
            trk = trks{idxTrk};
            trkFName = getTrackerFancyName(trk);

            % Look if the tracker specifies its number of runs
            numRunsTrk = getFieldDefault(trk, 'runs', numRuns);

            fprintf('  Simulation on sequence ''%s'' with tracker ''%s'' (%d runs)\n',...
                    seqName, trkFName, numRunsTrk);

            % Set pretty tracker name and snake-case tracker name
            trkFName = getTrackerFancyName(trk);
            trkSName = getTrackerSnakeName(trk);

            % Get absolute path for results
            fileName = getResultsFileName(seq.name, trkSName);
            dataFilePath = fullfile(cd(cd(resultsPath)), fileName);

            firstRun = 0;
            allTrkResults = {};
            durations = [];

            % The results file already exists; should we overwrite, resume, skip?
            if isfield(parameters, 'dryrun') && meanYes(parameters.dryrun)
                fprintf('  Dry run, no data will be saved!\n');
            else
                if exist(dataFilePath, 'file')
                    if (isfield(trk, 'overwrite') && meanYes(trk.overwrite)) ||...
                            (isfield(parameters, 'overwrite') && meanYes(parameters.overwrite))
                        fprintf('  Overwriting existing results\n');
                    else
                        load(dataFilePath, 'fullResult');
                        allTrkResults = fullResult.trkResults;
                        durations = fullResult.durations;
                        firstRun = length(fullResult.trkResults);
                        if firstRun < numRunsTrk
                            fprintf('  Resuming %d runs already done\n', length(allTrkResults));
                        else
                            fprintf('  Enough number of runs: %d/%d\n', firstRun, numRunsTrk);
                            numTask = numTask + firstRun;
                            continue;
                        end
                    end
                end
            end

            % Function name used to call the tracker
            funcName = getFieldDefault(trk, 'func', ['run_', trk.name]);
            func = str2func(funcName);

            % Calling convention of the tracker
            callingConvention = getFieldDefault(trk, 'callingConvention', 'bentta');

            % cd to path of tracker
            if isfield(trk, 'path')
                cd(trk.path);
            elseif exist(fullfile(trackersPath, trk.name), 'dir')
                cd(fullfile(trackersPath, trk.name));
            end

            numTask = numTask + firstRun;
            for idxRun = firstRun+1:numRunsTrk
                fprintf('  Tracker ''%s'', run number %d/%d, overall (%d/%d)\n',...
                        trkFName, idxRun, numRunsTrk, numTask, sum(numTaskTotal));
                tic

                % To catch or not to catch?
                if meanYes(getFieldDefault(parameters, 'debugOnError', false))
                    callTracker();
                else
                    try
                        callTracker();
                    catch err
                        fprintf('  Error in tracker ''%s'': \n%s\n', trkFName, getReport(err));
                        allTrkResults = {};
                        break;
                    end
                end

                % Store results
                allTrkResults{idxRun} = trkResult;
                durations(idxRun) = toc;

                % Save results for this run if next fails
                if ~(isfield(parameters, 'dryrun') && meanYes(parameters.dryrun))
                    clear fullResult;
                    fullResult.durations = durations;
                    fullResult.trkResults = allTrkResults;
                    fullResult.sequence = seq;
                    fullResult.tracker = trk;
                    save(dataFilePath, 'fullResult');
                end

                numTask = numTask + 1;
            end

            cd(currentPath);

            if isfield(parameters, 'dryrun') && meanYes(parameters.dryrun)
                fprintf('  Dry run, no data saved\n');
            else
                if length(allTrkResults) == 0
                    fprintf('  No data saved\n');
                elseif length(allTrkResults) < numRunsTrk
                    fprintf('  Saving %d out of %d runs\n', length(allTrkResults), numRunsTrk);
                    save(dataFilePath, 'fullResult');
                else
                    fprintf('  Results written in ''%s''\n', dataFilePath);
                    save(dataFilePath, 'fullResult');
                end
            end
        end
    end

    % Compute overlap and centre error
    computePerfs(smls, seqs, trksAll, parameters);

    function callTracker
        if strcmp(lower(callingConvention), 'wu')
            % Pass trk as fourth argument if possible
            if nargin(func) < -3 || nargin(func) == 4
                trkResult = func(seq, '', 0, trk);
            elseif nargin(func) == 3
                trkResult = func(seq, '', 0);
            else
                error('Wrong number of arguments for ''%s''\n', funcName);
            end
        else
            if nargin(func) == 1
                trkResult = func(seq);
            elseif nargin(func) == 2
                trkResult = func(seq, trk);
            else
                error('Wrong number of arguments for ''%s''\n', funcName);
            end
        end
    end
end

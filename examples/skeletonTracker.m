function results = skeletonTracker(seq, trk)

% This is a sleleton of a tracker following the default calling
% convention. It requires two different arguments: the sequence to use
% and the tracker itself as described in the README.

% Get paths of images
frames = seq.frames;

% Get first bounding box
initRect = seq.initRect;

for i = 1:length(frames)
    im = imread(frames{i});

    % Track
end

% Preparing results
results.res = <bounding boxes>;

% Bounding box are of rectangular type
results.type = 'rect'

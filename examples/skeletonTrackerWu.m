function results=skeletonTrackerWu(seq, res_path, bSaveImage)

% This is a sleleton of a tracker following the calling convention
% from Wu et al (see README). It requires the path of frames stored in
% the field S_FRAMES of SEQ, RES_PATH and BSAVEIMAGE are used to write
% results down.

% Get paths of images
frames = seq.s_frames;

% Get first bounding box
initRect = seq.initRect;

for i = 1:length(frames)
    im = imread(frames{i});

    % Track
end

% Preparing results
results.res = <bounding boxes>;

% Bounding box are of rectangular type
results.type = 'rect'

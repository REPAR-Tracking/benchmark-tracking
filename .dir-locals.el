((nil
  (vc-check (unpushed "master"))
  (org-context-capture
   ("t" "Todo" entry
    (file "todo.org")
    "* TODO %?\n  OPENED: %U"))
  (org-context-agenda
   ("t" "TODO" alltodo ""
    ((org-agenda-files '("todo.org")))))))

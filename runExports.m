function runExports(expts, seqs, trksAll, varargin)
%RUNEXPORTS Perform the configured exports.
% The list of exporting tasks is specified by EXPTS. SEQS in the set
% of available sequences and TRKSALL the set of available trackers. An
% optional parameter that mainly stores paths can be supplied.

% Location of this file
p = mfilename('fullpath');
path = fileparts(p);
addpath(fullfile(path, 'utils'));
addpath(fullfile(path, 'utils', 'vendor'));

if length(varargin) > 0
    parameters = varargin{1};
else
    parameters = struct;
end

% Get various path from PARAMETERS
resultsPath = getFieldDefault(parameters, 'resultsPath', './results');
perfsPath = getFieldDefault(parameters, 'perfsPath', './perfs');
framesPath = getFieldDefault(parameters, 'framesPath', './frames');
plotsPath = getFieldDefault(parameters, 'plotsPath', './plots');
videosPath = getFieldDefault(parameters, 'videosPath', './videos');

% ...and store them in PARAMETERS if not present
parameters.resultsPath = resultsPath;
parameters.perfsPath = perfsPath;
parameters.framesPath = framesPath;
parameters.plotsPath = plotsPath;
parameters.videosPath = videosPath;

exist(framesPath, 'dir') || mkdir(framesPath);
exist(plotsPath, 'dir') || mkdir(plotsPath);
exist(videosPath, 'dir') || mkdir(videosPath);

expts_flatten = {};
for i = 1:length(expts)
    expts_flatten = [expts_flatten, num2cell(expts{i})];
end
expts = expts_flatten;

fprintf('%d exports configured\n', length(expts));

for idxExpt = 1:length(expts)
    expt = expts{idxExpt};
    exptName = getFieldDefault(expt, 'name', ['Export ', num2str(idxExpt)]);
    fprintf('Exporting export ''%s''\n', exptName);

    runExport(expt, seqs, trksAll, parameters);
end


function computePerfs(smls, seqs, trksAll, parameters)
%COMPUTEPERFS Compute the performances of trackers from raw results.
% The simulations are stored in SMLS. SEQS in the set of available
% sequences and TRKSALL the set of available trackers. PARAMETERS
% stores the path of raw results in the field resultsPath and the path
% for performance data in field perfsPath.

% Get various path from PARAMETERS
resultsPath = getFieldDefault(parameters, 'resultsPath', './results');
perfsPath = getFieldDefault(parameters, 'perfsPath', './perfs');

% All possible simulations if no simulation
if length(smls) == 0
    smls = {};
    for i = 1:length(seqs)
        smls{i}.sequence = seqs{i}.name;
    end
end

% Flatten simulations in case one simulation struct is an array of
% struct
smls_flatten = {};
for i = 1:length(smls)
    smls_flatten = [smls_flatten, num2cell(smls{i})];
end
smls = smls_flatten;

disp('Computing performances');

for idxSml = 1:length(smls)
    sml = smls{idxSml};
    seqName = sml.sequence;

    fprintf('  Computing performances on sequence ''%s''\n', seqName);

    seq = getSequenceFromName(seqName, seqs);
    if ~isstruct(seq)
        fprintf('  Sequence ''%s'' not found, skipping\n', seqName)
        break;
    end

    % Default format of image files
    if ~isfield(seq, 'format')
        seq.format = '%04d.jpg';
    end

    % Start at frame number 1 by default
    if ~isfield(seq, 'startFrame')
        seq.startFrame = 1;
    end

    if ~isfield(seq, 'endFrame')
        seq.endFrame = getEndFrame(seq);
    end
    seq.len = seq.endFrame - seq.startFrame + 1;

    if ~isfield(seq, 'anno')
        fprintf('  Annotation file for sequence ''%s'' not found, skipping\n', seqName);
        continue
    end

    % Groundtruth rectangles
    rectAnno = dlmread(seq.anno);
    if seq.endFrame > size(rectAnno, 1)
        fprintf('  WARNING: endFrame too large (%d), setting it to: %d\n', seq.endFrame, size(rectAnno, 1));
        seq.endFrame = size(rectAnno, 1);
    end
    rectAnno = rectAnno(seq.startFrame:seq.endFrame, :);

    % Compute associated trackers
    trks = getUsedTrackers(sml, trksAll);

    for idxTrk = 1:length(trks)
        trk = trks{idxTrk};

        % Set pretty tracker name and snake-case tracker name
        trkFName = getTrackerFancyName(trk);
        trkSName = getTrackerSnakeName(trk);
        fprintf('  Performances on sequence ''%s'' with tracker ''%s''\n', seqName, trkFName);

        % Load rectangles from simulation
        dataFilename = getResultsFileName(seq.name, trkSName);
        if ~exist(fullfile(resultsPath, dataFilename), 'file')
            fprintf('  WARNING: Results file not found: %s, skipping\n',...
                    fullfile(resultsPath, dataFilename));
            continue;
        end
        load(fullfile(resultsPath, dataFilename), 'fullResult');

        if isfield(fullResult.trkResults{1}, 'res')
            if size(fullResult.trkResults{1}.res, 1) ~= size(rectAnno, 1)
                error('Configured length of sequence changed after simulations, skipping');
            end

            % Compute the stats
            overlapRate = [];
            errCenter = [];
            for idxRun = 1:length(fullResult.trkResults)
                results = fullResult.trkResults{idxRun};
                [overlapRate0, errCenter0] = computeStats(results, rectAnno);
                overlapRate = [overlapRate; overlapRate0];
                errCenter = [errCenter; errCenter0];
            end

            % Save overlap rate and error center in perfsPath
            perfsFilename = fullfile(perfsPath, ['overlapRate_', dataFilename]);
            save(perfsFilename, 'overlapRate');

            perfsFilename = fullfile(perfsPath, ['errCenter_', dataFilename]);
            save(perfsFilename, 'errCenter');
        else
            fprintf('  WARNING: No field ''res'' in trackers results\n');
        end
    end
end

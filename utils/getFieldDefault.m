function res = getFieldDefault(s, field, default)
%GETFIELDDEFAULT Return field value from structure.
% RES = GETFIELDDEFAULT(S, FIELD, DEFAULT) returns the value of field
% FIELD in structure S or DEFAULT if not present.

if isfield(s, field)
    res = s.(field);
else
    res = default;
end

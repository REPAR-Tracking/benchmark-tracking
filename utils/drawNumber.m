function drawNumber(expt, idxFrame)
%DRAWNUMBER Draw the number IDXFRAME on the current figure.

if isfield(expt, 'number') && meanYes(expt.number)
    extraTextArgs = getFieldDefault(expt, 'extraTextArgs', {});
    text(1, 1, ['#', num2str(idxFrame)],...
         'HorizontalAlignment', 'left',...
         'VerticalAlignment', 'top',...
         'Color', 'y',...
         'FontWeight', 'bold',...
         'FontSize', 24,...
         extraTextArgs{:});
end

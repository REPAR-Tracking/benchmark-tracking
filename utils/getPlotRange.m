function range = getPlotRange(expt, seq, type)
%GETPLOTRANGE Get the range of the plot from its type.

switch type
  case 'successRateErrCenter'
    if isfield(expt, 'successRateErrCenterRange')
        range = expt.successRateErrCenter;
    else
        range = 0:.5:50;
    end
  case 'successRateOverlapRate'
    if isfield(expt, 'successRateOverlapRateRange')
        range = expt.successRateOverlapRateRange;
    else
        range = 0:.05:1;
    end
  case {'errCenter', 'overlapRate'}
    if isfield(expt, 'sequenceRange')
        range = max(expt.sequenceRange(1), seq.startFrame):...
                min(expt.sequenceRange(end), seq.endFrame);
    else
        range = seq.startFrame:seq.endFrame;
    end
  otherwise
    error('getPlotRange: Unknown type ''%s''', type);
end

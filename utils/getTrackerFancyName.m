function trkName = getTrackerFancyName(trk)
%GETTRACKERFANCYNAME Return name of tracker.
% NAME = GETTRACKERFANCYNAME(TRK) returns a fancy name for the tracker
% TRK as described in configTrackers.

if isfield(trk, 'variant')
    if isnumeric(trk.variant)
        trkName = sprintf('%s[%d]', trk.name, trk.variant);
    elseif ischar(trk.variant)
        trkName = sprintf('%s[%s]', trk.name, trk.variant);
    else
        error('Invalid variant name');
    end
else
    trkName = trk.name;
end

function seq = getSequenceFromName(seqName, seqs)
%GETSEQUENCEFROMNAME Return a sequence from its name.

seq = 'not found';

for i = 1:length(seqs)
    if strcmp(seqs{i}.name, seqName)
        seq = seqs{i};
        break;
    end
end

function legendLocation = getLegendLocation(type)
%GETLEGENDLOCATION Get legend location from the type of plot.

switch type
  case 'errCenter'
    legendLocation = 'northwest';
  case 'overlapRate'
    legendLocation = 'southwest';
  case 'successRateOverlapRate'
    legendLocation = 'southwest';
  case 'successRateErrCenter'
    legendLocation = 'southeast';
  otherwise
    error('getLegendLocation: Unknown type ''%s''', type);
end

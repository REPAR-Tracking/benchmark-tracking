function trk = getTrackerFromName(trkName, trks)
%GETTRACKERFROMNAME Return a tracker from its name.

trk = 'not found';
for i = 1:length(trks)
    if strcmp(trks{i}.name, trkName) ||...
            strcmp(getTrackerSnakeName(trks{i}), trkName) ||...
            strcmp(getTrackerFancyName(trks{i}), trkName)
        trk = trks{i};
        break;
    end
end

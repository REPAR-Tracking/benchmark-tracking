function [title, xLabel, yLabel] = getPlotLabels(expt, seq, type);
%GETPLOTLABELS Get the title, xlabel and ylabel of the plot from its
% type.

% Default title, xlabel and ylabel
switch lower(type)
  case 'successrateerrcenter'
    title = sprintf('Success rate of Centre Error on sequence %s', seq.name);
    xLabel = 'Threshold';
    yLabel = 'Success Rate';
  case 'successrateoverlaprate'
    title = sprintf('Success Rate of Overlap on sequence %s', seq.name);
    xLabel = 'Threshold';
    yLabel = 'Success Rate';
  case 'errcenter'
    title = sprintf('Centre Error on sequence %s', seq.name);
    xLabel = 'Frames';
    yLabel = 'Error in pixels';
  case 'overlaprate'
    title = sprintf('Overlap Rate on sequence %s', seq.name);
    xLabel = 'Frames';
    yLabel = 'Overlap';
  otherwise
    error('getPlotTitle: Unknown type ''%s''', type);
end

% Override settings in export
title = getFieldDefault(expt, 'title', title);
xLabel = getFieldDefault(expt, 'xLabel', xLabel);
yLabel = getFieldDefault(expt, 'yLabel', yLabel);

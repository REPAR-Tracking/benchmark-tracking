function usedSeqs = getUsedSequences(expt, seqs)
%GETUSEDSEQUENCES Return the sequences used in EXPT.

% Storing sequences associated with current export
if isfield(expt, 'sequences')
    if ischar(expt.sequences)
        seqNames = {expt.sequences};
    else
        seqNames = expt.sequences;
    end
    fprintf('  Using %d sequences\n', length(seqNames));
    usedSeqs = {};
    for idxSeq = 1:length(seqNames)
        seqName = seqNames{idxSeq};
        seq = getSequenceFromName(seqName, seqs);
        if ~isstruct(seq)
            fprintf('  Sequence ''%s'' not found, not using it\n', seqName);
            continue
        end
        usedSeqs = [usedSeqs, seq];
    end
else
    fprintf('  Using all %d sequences\n', length(seqs));
    usedSeqs = seqs;
end

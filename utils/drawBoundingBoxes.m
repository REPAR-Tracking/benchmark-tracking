function drawBoundingBoxes(expt, seq, validTrks, idxs, boundingBoxes, idxFrame, plotDrawStyle)
%DRAWBOUNDINGBOXES Draw bounding boxes on current figure.

% Maximum number of curves for current plot
trkMax = getFieldDefault(expt, 'trackersMax', +Inf);
extraRectangleArgs = getFieldDefault(expt, 'extraRectangleArgs', {});
extraRectangleArgsSeq = getFieldDefault(seq, 'extraRectangleArgsSeq', {});

useGT = isfield(expt, 'groundtruth') && meanYes(expt.groundtruth);
if useGT
    try
        rectAnno = dlmread(seq.anno);
    catch
        fprintf('  Unable to load groundtruth, disabling\n');
        useGT = false;
    end
end

% Draw overlay rectangle for groundtruth
if useGT
    GTShift = 1;
    rect = rectAnno(idxFrame, :);

    % Sanitize rectangle
    rect = [rect(1), rect(2), max(rect(3), 1), max(rect(4), 1)];

    color = getFieldDefault(seq, 'color', plotDrawStyle{1}.color);
    style = getFieldDefault(seq, 'style', plotDrawStyle{1}.lineStyle);

    rectangle('Position', rect,...
              'EdgeColor', color,...
              'LineStyle', style,...
              'LineWidth', 2,...
              extraRectangleArgs{:},...
              extraRectangleArgsSeq{:});
else
    GTShift = 0;
end

% Draw overlay rectangle for all trackers
for idxTrk = 1:min(length(validTrks), trkMax)
    idxSortTrk = idxs(idxTrk);
    trk = validTrks{idxSortTrk};

    extraRectangleArgsTrk = getFieldDefault(trk, 'extraRectangleArgs', {});

    if ~isfield(boundingBoxes{idxSortTrk}, 'type')
        boundingBoxes{idxSortTrk}.type = 'rect';
    end
    switch boundingBoxes{idxSortTrk}.type
      case 'rect'
        rect = boundingBoxes{idxSortTrk}.res(idxFrame, :);
      case 'ivtAff'
        results = boundingBoxes{idxSortTrk};
        rect = calcRectCenter(results.tmplsize, results.res(idxFrame, :));
      otherwise
        error('Bounding box of type ''%s'' not supported', type);
    end

    % Sanitize rectangle
    rect = [rect(1), rect(2), max(rect(3), 1), max(rect(4), 1)];

    color = getFieldDefault(trk, 'color', plotDrawStyle{idxSortTrk+GTShift}.color);
    style = getFieldDefault(trk, 'style', plotDrawStyle{idxSortTrk+GTShift}.lineStyle);

    rectangle('Position', rect,...
              'EdgeColor', color,...
              'LineStyle', style,...
              'LineWidth', 2,...
              extraRectangleArgs{:},...
              extraRectangleArgsTrk{:});
end

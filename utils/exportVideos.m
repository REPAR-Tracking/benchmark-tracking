function exportVideos(expt, seq, validTrks, idxs, boundingBoxes, parameters)
%EXPORTVIDEOS Export videos with bounding boxes and number.

videosPath = parameters.videosPath;
plotDrawStyle = getFieldDefault(parameters, 'plotDrawStyle', getPlotDrawStyle());

% Sanitize range
if isfield(expt, 'videoRange')
    % Make the range contiguous
    range = max(expt.videoRange(1), seq.startFrame):...
            min(expt.videoRange(end), seq.endFrame);
else
    % Full range by default
    range = seq.startFrame:seq.endFrame;
end

fprintf('    Exporting video for sequence ''%s''\n', seq.name);
fig = figure;

% Get video name and its file path
fileName = getFieldDefault(expt, 'videoName', getFieldDefault(expt, 'figName', seq.name));
filePath = fullfile(videosPath, [fileName, '.avi']);

writerObj = VideoWriter(filePath, 'Uncompressed AVI');
open(writerObj);

for idxRange = 1:length(range)
    idxFrame = range(idxRange);
    img = imread(fullfile(seq.path, sprintf(seq.format, idxFrame)));
    imshow(img);

    % Print number at top left of frame
    drawNumber(expt, idxFrame);

    % Print bounding boxes of all trackers
    drawBoundingBoxes(expt, seq, validTrks, idxs, boundingBoxes, idxFrame, plotDrawStyle);

    % Get modified image; make sure it's of same size
    img_frame = frame2im(getframe);
    img_crop = img_frame(1:size(img, 1), 1:size(img, 2), :);

    writeVideo(writerObj, img_crop);
end
close(writerObj);
close(fig);

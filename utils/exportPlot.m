function exportPlot(expt, seq, trks, parameters, type)
%EXPORTPLOT Exports a plot of type TYPE.

% Get range of plot from its type
range = getPlotRange(expt, seq, type);

% Filter out trackers and compute plot data
[plotData, validTrks] = getPlotData(expt, seq, trks, range, parameters, type);

% Get legend names and regular names
[trkNames, legendNames] = getTrackerLegendNames(validTrks);

if length(validTrks) == 0
    disp('    Empty plot, skipping');
else
    fprintf('    Exporting with trackers ''%s''\n',  strjoin(trkNames, ', '));

    idxs = getSortingStrategy(expt, plotData, type);

    plotDrawStyle = getFieldDefault(parameters, 'plotDrawStyle', getPlotDrawStyle());

    % Maximum number of curves for current plot
    trkMax = getFieldDefault(expt, 'trackersMax', +Inf);

    % Is there a shift introduced by the groundtruth
    if isfield(expt, 'groundtruth') && meanYes(expt.groundtruth)
        GTShift = 1;
    else
        GTShift = 0;
    end

    % Drawing TRKMAX curves
    fig = figure;
    hold on;
    legendNamesMax = {};

    extraPlotArgs = getFieldDefault(expt, 'extraPlotArgs', {});
    for i = 1:min(size(plotData, 1), trkMax);
        legendNamesMax{i} = legendNames{idxs(i)};
        trk = validTrks{idxs(i)};
        trkColor = getFieldDefault(trk, 'color', plotDrawStyle{idxs(i)+GTShift}.color);
        trkStyle = getFieldDefault(trk, 'lineStyle', plotDrawStyle{idxs(i)+GTShift}.lineStyle);
        plot(range, plotData(idxs(i), :),...
             'color', trkColor,...
             'lineStyle', trkStyle,...
             'lineWidth', 2,...
             extraPlotArgs{:});
    end

    % Get default font size
    fontSize = getFieldDefault(expt, 'fontSize', 10);

    [theTitle, xLabel, yLabel] = getPlotLabels(expt, seq, type);

    % Set title of plot
    title(theTitle, 'fontsize', fontSize);

    % Set x and y labels
    extraLabelName = getFieldDefault(expt, 'extraLabelName', {});
    extraxLabelName = getFieldDefault(expt, 'extraxLabelName', {});
    extrayLabelName = getFieldDefault(expt, 'extrayLabelName', {});
    xlabel(xLabel, 'fontsize', fontSize, extraLabelName{:}, extraxLabelName{:});
    ylabel(yLabel, 'fontsize', fontSize, extraLabelName{:}, extrayLabelName{:});

    % Set legend
    extraLegendArgs = getFieldDefault(expt, 'extraLegendArgs', {});
    legendLocation = getFieldDefault(expt, 'legendLocation', getLegendLocation(type));
    legend(legendNamesMax, 'Interpreter', 'none',...
           'fontsize', fontSize,...
           'Location', legendLocation,...
           extraLegendArgs{:});

    hold off;

    if isfield(expt, 'export')
        % Cellify to iterate
        if ischar(expt.export)
            expts = {expt.export};
        elseif iscell(expt.export)
            expts = expt.export;
        else
            error('Unknown export');
        end

        plotsPath = parameters.plotsPath;
        figName = getFieldDefault(expt, 'figName', [type, '_', seq.name]);

        for i = 1:length(expts)
            switch lower(expts{i})
              case {'png', 'jpg', 'jpeg', 'tiff'}
                disp('    Exporting to image file');
                saveas(gcf, fullfile(plotsPath, figName), expts{i});
              case 'tikz'
                extraTikzArgs = getFieldDefault(expt, 'extraTikzArgs', {});
                disp('    Exporting with matlab2tikz');
                warning off matlab2tikz:deprecatedEnvironment
                matlab2tikz('showInfo', false,...
                            extraTikzArgs{:},...
                            fullfile(plotsPath, [figName, '.tex']));
              otherwise
                fprintf('    Unknown export ''%s'', skipping this export\n', expts{i});
                continue
            end
        end
        close(fig);
    end
end

function trkName = getTrackerSnakeName(trk)
%GETTRACKERSNAKENAME Return the snake name of a tracker.
% NAME = GETTRACKERSNAKENAME(TRK) returns a name with underscore to be
% used as part of a filename.

if isfield(trk, 'variant')
    if isnumeric(trk.variant)
        trkName = [trk.name, '_', num2str(trk.variant)];
    elseif ischar(trk.variant)
        trkName = [trk.name, '_', trk.variant];
    else
        error('Invalid variant name');
    end
else
    trkName = trk.name;
end

function [trkNames, legendNames] = getTrackerLegendNames(validTrks)
%GETTRACKERLEGENDNAMES Return fancy names and names to appear as
%legend of VALIDTRKS.

trkNames = {};
legendNames = {};
for idxTrk = 1:length(validTrks)
    trk = validTrks{idxTrk};

    trkFName = getTrackerFancyName(trk);
    trkNames{idxTrk} = trkFName;

    legendName = getFieldDefault(trk, 'legendName', trkFName);
    legendNames{idxTrk} = legendName;
end

function endFrame = getEndFrame(seq)
%GETENDFRAME Guess last frame of sequence SEQ from number of images.

% Guess number of frames
num = 0;
seqPath = getFieldDefault(seq, 'path', seq.name);
while exist(fullfile(seqPath, sprintf(seq.format, num + seq.startFrame)))
    num = num + 1;
end
if num <= 0, error('Unable to find frames'); end

endFrame = seq.startFrame + num - 1;

function runExport(expt, seqs, trksAll, parameters)
%RUNEXPORT Run appropriate exporting function.

if iscell(expt.type)
    exptTypes = expt.type;
else
    exptTypes = {expt.type};
end

for idxType = 1:length(exptTypes)
    exptType = exptTypes{idxType};

    switch lower(exptType)
      case {'errcenter', 'overlaprate', 'successrateerrcenter', 'successrateoverlaprate', 'video', 'frame', 'successrateoverlap'}

        if ~isfield(expt, 'sequence')
            disp('  No field ''sequence'' in this export, skipping');
            return
        end

        seqName = expt.sequence;
        seq = getSequenceFromName(seqName, seqs);
        if ~isstruct(seq)
            fprintf('  Unknown sequence ''%s'', skipping\n', seqName);
            return
        end

        fprintf('  Sub-export of type ''%s'' on ''%s''\n', exptType, seqName);

        % Default format of image files
        if ~isfield(seq, 'format')
            seq.format = '%04d.jpg';
        end

        % Start at frame number 1 by default
        if ~isfield(seq, 'startFrame')
            seq.startFrame = 1;
        end

        if ~isfield(seq, 'endFrame')
            seq.endFrame = getEndFrame(seq);
        end
        seq.len = seq.endFrame - seq.startFrame + 1;

        % Storing trackers associated with current export
        trks = getUsedTrackers(expt, trksAll);
        if length(trks) == 0 && ~strcmp('video', lower(exptType)) && ~strcmp('frame', lower(exptType))
            disp('    No tracker configured, skipping this export');
            return
        end

        switch lower(exptType)
          case 'errcenter'
            exportPlot(expt, seq, trks, parameters, 'errCenter');
          case 'overlaprate'
            exportPlot(expt, seq, trks, parameters, 'overlapRate');
          case 'successrateerrcenter'
            exportPlot(expt, seq, trks, parameters, 'successRateErrCenter');
          case {'successrateoverlap', 'successrateoverlaprate'}
            exportPlot(expt, seq, trks, parameters, 'successRateOverlapRate');
          case 'video'
            [validTrks, idxs, boundingBoxes] = prepareFrame(expt, seq, trks, parameters);
            exportVideos(expt, seq, validTrks, idxs, boundingBoxes, parameters);
          case 'frame'
            [validTrks, idxs, boundingBoxes] = prepareFrame(expt, seq, trks, parameters);
            exportFrames(expt, seq, validTrks, idxs, boundingBoxes, parameters);
        end
      otherwise
        funcName = ['export', exptType];
        if exist(funcName)
            fprintf('  Found custom exporter function ''%s'' in path, calling it\n', funcName);
            func = str2func(funcName);
            try
                func(expt, seqs, trksAll, parameters, exptType);
            catch err
                fprintf('    Error in export ''%s'': \n%s\n', funcName, getReport(err));
            end
        else
            fprintf('  Unknown export ''%s'', skipping this export\n', exptType);
        end
    end
end

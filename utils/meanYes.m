function res = meanYes(obj)
%MEANYES Return true if OBJ represent a true object.
% B = MEANYES(OBJ) returns true if OBJ is a string that is not 'no',
% OBJ if OBJ is a boolean, true if OBJ is a non-negative real number.
% Otherwise, returns false.

if ischar(obj)
    res = ~strcmp(lower(obj), 'no');
elseif islogical(obj)
    res = obj;
elseif isreal(obj)
    res = obj > 0;
else
    res = false;
end

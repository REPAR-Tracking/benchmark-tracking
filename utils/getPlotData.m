function [plotData, validTrks] = getPlotData(expt, seq, trks, range, parameters, type)
%GETPLOTDATA Return data for plot.

perfsPath = parameters.perfsPath;

switch type
  case {'errCenter', 'successRateErrCenter'}
    subtype = 'errCenter';
  case {'overlapRate', 'successRateOverlapRate'}
    subtype = 'overlapRate';
  otherwise
    error('exportSuccessPlots: Unknown type ''%s''', type);
end

if isfield(expt, 'sequenceRange')
    seqRange = max(expt.sequenceRange(1), seq.startFrame):...
            min(expt.sequenceRange(end), seq.endFrame);
else
    seqRange = seq.startFrame:seq.endFrame;
end

plotData = [];
validTrks = {};
k = 1;
for idxTrk = 1:length(trks);
    trk = trks{idxTrk};
    trkSName = getTrackerSnakeName(trk);
    trkFName = getTrackerFancyName(trk);

    % Storing data for each configured tracker in plotData
    baseName = [subtype, '_', seq.name, '_', trkSName, '.mat'];
    fileName = fullfile(perfsPath, baseName);

    try
        % Loading the right data
        results = load(fileName, subtype);
        y = results.(subtype);

        % Selecting the wanted runs from the data
        if isfield(expt, 'selectedRuns')
            y_runs = y(expt.selectedRuns, :);
        else
            y_runs = y;
        end

        % Select the wanted frames
        y_select = y_runs(:, seqRange - seq.startFrame + 1);

        % Process data and average over multiple runs
        switch type
          case 'errCenter'
            y = mean(y_select, 1);
          case 'overlapRate'
            y = mean(y_select, 1);
          case 'successRateOverlapRate'
            y_mean = mean(y_select, 1);
            clear y;
            for i = 1:length(range)
                y(i) = mean(y_mean(:) >= range(i));
            end
          case 'successRateErrCenter'
            y_mean = mean(y_select, 1);
            clear y;
            for i = 1:length(range)
                y(i) = mean(y_mean(:) <= range(i));
            end
          otherwise
            error('getPlotData: Unknown type ''%s''', type);
        end
        plotData(k, :) = y;
    catch
        fprintf('  Unable to load file ''%s'', skipping\n', fileName);
        continue;
    end

    validTrks{k} = trk;
    k = k + 1;
end

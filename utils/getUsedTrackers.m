function [trks, trkNames, notFound] = getUsedTrackers(expt, trksAll)
%GETUSEDTRACKERS Return the trackers used in EXPT.

% Storing trackers associated with current export
if isfield(expt, 'trackers')
    if iscell(expt.trackers)
        trkNames = expt.trackers;
    else
        trkNames = {expt.trackers};
    end
    trks = {};
    trkUsedNames = {};
    notFound = {};
    for idxTrk = 1:length(trkNames)
        trkName = trkNames{idxTrk};
        trk = getTrackerFromName(trkName, trksAll);
        if ~isstruct(trk)
            notFound = [notFound, trk];
            continue
        end
        trks = [trks, trk];
        trkUsedNames = [trkUsedNames, trkName];
    end
else
    notFound = {};
    trks = trksAll;
    trkNames = cellfun(@(x) getTrackerFancyName(x), trks, 'UniformOutput', false);
end

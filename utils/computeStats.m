function [overlapRate, errCenter] = computeStats(results, gtRects)
%COMPUTESTATS Compute bounding box statistics.
% [OR, EC] = COMPUTESTATS(R, GTRECTS) returns the overlapping rate and
% the error center between R and GTRECTS.
%
% R is a structure with field RESULTS and TYPE. RESULTS is an array of
% size N*P where N is the number of frames and P depends on TYPE. For
% example, if TYPE is 'rect', P=4.
%
% GTRECTS is the array of groundtruth results of size N*4.

% Groundtruth centers
gtCenters = [gtRects(:, 1) + (gtRects(:, 3) - 1)/2,...
             gtRects(:, 2) + (gtRects(:, 4) - 1)/2];

len = size(gtRects, 1);

% Candidate rectangles
switch results.type
  case 'rect'
    % Same rectangle for first frame
    cRects = results.res;
    cRects(1,:) = gtRects(1,:);
  case 'ivtAff'
    for i = 1:len
        [rect c] = calcRectCenter(results.tmplsize, results.res(i, :));
        cRects(i,:) = rect;
    end
end

% Candidate centers
cCenters = [cRects(:,1)+(cRects(:,3)-1)/2 cRects(:,2)+(cRects(:,4)-1)/2];

% Euclidean distance between the two centers and average distance
errCenter = sqrt(sum(((cCenters(1:len,:) - gtCenters(1:len,:)).^2), 2));
errCenter = errCenter';

% Areas of groundtruth and candidate rectangles
cAreas = abs(cRects(:, 3) .* cRects(:, 4));
gtAreas = abs(gtRects(:, 3) .* gtRects(:, 4));

% Intersections of groundtruth and candidate rectangles
gtRects = max(gtRects, 0); % Octave compat
cRects = max(cRects, 0); % Octave compat
inters = diag(rectint(gtRects, cRects));

% Computing overlapping ratio and average overlapping
overlapRate = inters ./ (cAreas + gtAreas - inters);
overlapRate = overlapRate';

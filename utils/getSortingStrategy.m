function idxs = getSortingStrategy(expt, plotData, type)
%GETSORTINGSTRATEGY Return the order of plots.

switch type
  case 'errCenter'
    bestFirst = 'ascend';
    worstFirst = 'descend';
  case 'overlapRate'
    bestFirst = 'descend';
    worstFirst = 'ascend';
  case 'successRateOverlapRate'
    bestFirst = 'descend';
    worstFirst = 'ascend';
  case 'successRateErrCenter'
    bestFirst = 'descend';
    worstFirst = 'ascend';
  otherwise
    error('    getSortingStrategy: Unknown type: ''%s''', type);
end

% Sort plot from better to worse
if isfield(expt, 'sort')
    switch expt.sort
      case 'best first'
        disp('    Best first sorting');
        [~, idxs] = sort(sum(plotData, 2), bestFirst);
      case 'worst first'
        disp('    Worst first sorting');
        [~, idxs] = sort(sum(plotData, 2), worstFirst);
      otherwise
        disp('    No sorting');
        idxs = 1:size(plotData, 1); % No sorting
    end
else
    disp('    Best first sorting');
    [~, idxs] = sort(sum(plotData, 2), bestFirst);
end

function exportFrames(expt, seq, validTrks, idxs, boundingBoxes, parameters)
%EXPORTFRAMES Export frames with bounding boxes and number.

framesPath = parameters.framesPath;
plotDrawStyle = getFieldDefault(parameters, 'plotDrawStyle', getPlotDrawStyle());

% Allow single index or array of indexes
if isfield(expt, 'frameRange')
    if isnumeric(expt.frameRange)
        frameRange = [expt.frameRange];
    else
        frameRange = expt.frameRange;
    end

    % Consistent set of indexes: not off-bounds and no dups
    range = max(frameRange, seq.startFrame);
    range = min(range, seq.endFrame);
    range = unique(range);
else
    % Whole set of indexes by default
    range = seq.startFrame:seq.endFrame;
end

fig = figure;
extraRectangleArgsSeq = getFieldDefault(seq, 'extraRectangleArgsSeq', {});
for idxRange = 1:length(range)
    idxFrame = range(idxRange);
    img = imread(fullfile(seq.path, sprintf(seq.format, idxFrame)));
    iptsetpref('ImshowInitialMagnification', 100); % Don't resize image
    imshow(img);
    fprintf('    Exporting frame %d of sequence ''%s''\n', idxFrame, seq.name);

    % Print number at top left of frame
    drawNumber(expt, idxFrame);

    % Print bounding boxes of all trackers
    drawBoundingBoxes(expt, seq, validTrks, idxs, boundingBoxes, idxFrame, plotDrawStyle);

    % Interpolate figName if there is a '%'
    if isfield(expt, 'figName')
        if ~isempty(strfind(expt.figName, '%')) &&...
                isempty(strfind(expt.figName, '%%'))
            % Interpolate: figName should contain an extension
            fileName = sprintf(expt.figName, idxFrame);
        else
            % Use figName and format of sequence
            fileName = [expt.figName, '_', sprintf(seq.format, idxFrame)];
        end
    else
        % Use sequence name and format of sequence
        fileName = [seq.name, '_', sprintf(seq.format, idxFrame)];
    end
    filePath = fullfile(framesPath, fileName);

    % Get modified image; make sure it's of same size
    img_frame = frame2im(getframe);
    img_crop = img_frame(1:size(img, 1), 1:size(img, 2), :);
    imwrite(img_crop, filePath);
end

close(fig)

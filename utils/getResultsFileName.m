function fileName = getResultsFileName(seqName, trkSName)
%GETRESULTSFILENAME Return the filename used to store the raw results.

fileName = [seqName, '_', trkSName, '.mat'];

function [validTrks, idxs, boundingBoxes] = prepareFrame(expt, seq, trks, parameters)
%PREPAREIMAGE Compute order, trackers and associated bounding boxes
% for frames and videos exporting.

resultsPath = parameters.resultsPath;
perfsPath = parameters.perfsPath;

% Selecting valid trackers
boundingBoxes = [];
validTrks = {};
validNames = {};
k = 1;
for idxTrk = 1:length(trks);
    trk = trks{idxTrk};
    trkSName = getTrackerSnakeName(trk);
    trkFName = getTrackerFancyName(trk);
    baseName = getResultsFileName(seq.name, trkSName);
    fileName = fullfile(resultsPath, baseName);

    try
        load(fileName, 'fullResult');
        if meanYes(getFieldDefault(expt, 'average', 'yes'))
            runs = getFieldDefault(expt, 'selectedRuns',...
                                         1:length(fullResult.trkResults));
            avgBoundingBoxes = zeros(size(fullResult.trkResults{1}.res));
            for run = runs
                avgBoundingBoxes = avgBoundingBoxes + ...
                    fullResult.trkResults{run}.res;
            end
            avgBoundingBoxes = avgBoundingBoxes / length(runs);
            bb = fullResult.trkResults{1};
            bb.res = avgBoundingBoxes;
            boundingBoxes{k} = bb;
        else
            boundingBoxes{k} = fullResult.trkResults{1};
        end
    catch
        fprintf('    Unable to load file ''%s'', skipping\n', fileName);
        continue;
    end

    validTrks{k} = trk;
    validNames{k} = trkFName;
    k = k + 1;
end

if k == 1
    disp('    No tracker left');
    idxs = [];
    return;
else
    fprintf('    Exporting with trackers ''%s''\n',  strjoin(validNames, ', '));
end

% Computing score for sorting
scores = [];
if isfield(expt, 'sort')
    switch expt.sort
      case {'best center error first', 'worst center error first', ...
            'best overlap first', 'worst overlap first'}
        switch expt.sort
          case 'best center error first'
            sortType = 'errCenter';
            direction = 'ascend';
          case 'worst center error first'
            sortType = 'errCenter';
            direction = 'descend';
          case 'best overlap first'
            sortType = 'overlapRate';
            direction = 'descend';
          case 'worst overlap first'
            sortType = 'overlapRate';
            direction = 'ascend';
        end

        for idxTrk = 1:length(validTrks);
            trk = trks{idxTrk};
            trkSName = getTrackerSnakeName(trk);
            baseName = [sortType, '_', seq.name, '_', trkSName, '.mat'];
            fileName = fullfile(perfsPath, baseName);

            try
                results = load(fileName, sortType);
                y = results.(sortType);
                if isfield(expt, 'selectedRuns')
                    y_mean = mean(y(expt.selectedRuns, :), 1);
                else
                    y_mean = mean(y, 1);
                end
                scores(idxTrk) = sum(y_mean);
            catch
                fprintf('    Unable to load file ''%s'', not sorting\n', fileName);
                scores = 1:length(validTrks);
                direction = 'ascend';
                break;
            end
        end

      otherwise
        fprintf('    Unknown sorting strategy: ''%s'', not sorting\n', expt.sort);

        % No sorting
        scores = 1:length(validTrks);
        direction = 'ascend';
    end
else
    % No sorting
    scores = 1:length(validTrks);
    direction = 'ascend';
end
[~, idxs] = sort(scores, direction);
